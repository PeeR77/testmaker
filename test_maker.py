import os
import argparse
from pathlib import Path

import CppHeaderParser


INTERSPACE = ' ' * 4


class TestMaker:
    def __init__(self, params):
        self.params = params
        self.file_or_path = params.file_or_dir
        self.header = None
        self.cpp_str = ''

        if os.path.isdir(self.file_or_path):
            self.create_tests_for_dir(self.file_or_path)
        elif os.path.isfile(args.file_or_dir):
            self.create_tests(args.file_or_dir)

    def get_output(self):
        return self.cpp_str

    def save_or_print(self, header_file):
        if self.params.output_path:
            file_name = os.path.basename(header_file)
            cpp_file_path = args.output_path + '/Test' + os.path.splitext(file_name)[0] + ".cpp"
            self.save_file(cpp_file_path, self.cpp_str)
        else:
            print(self.cpp_str)

    def create_tests_for_dir(self, path):
        for root, dirs, files in os.walk(path, topdown=False):
            for file in files:
                if file.endswith(".h") or file.endswith(".hpp"):
                    self.create_tests(os.path.join(root, file))

    def create_tests(self, header_file):
        self.header = CppHeaderParser.CppHeader(header_file)

        self.cpp_str = '#include <gtest/gtest.h>\n'
        self.cpp_str += '#include "{}"\n\n\n'.format(Path(self.header.headerFileName).name)

        class_list = self.header.classes_order
        for c in class_list:
            self.cpp_str += TestMaker.generate_test_class(c['name'])
            methods = c['methods']
            for method in methods['public']:
                if not method['constructor'] and not method['destructor']:
                    self.cpp_str += TestMaker.generate_test(c['name'], method)

        self.save_or_print(header_file)

    @staticmethod
    def generate_test_class(class_name):
        test_class_name = TestMaker.test_class_name(class_name)
        out_str = 'class {} : public testing::Test\n{{\nprotected:\n'.format(TestMaker.test_class_name(class_name))
        out_str += INTERSPACE + '{} {};\n'.format(class_name, TestMaker.test_class_object(class_name))
        out_str += '};\n\n'
        return out_str

    @staticmethod
    def generate_test(class_name, method):
        function_name = method['name']
        test_class_name = TestMaker.test_class_name(class_name)
        if '_' in function_name:
            test_function_name = 'test' + '_' + function_name
        else:
            test_function_name = 'test' + function_name[0].upper() + function_name[1:]

        test_content = TestMaker.generate_test_content(TestMaker.test_class_object(class_name), method)
        return 'TEST_F({}, {})\n{{\n{}\n}}\n\n'.format(test_class_name, test_function_name, test_content)

    @staticmethod
    def generate_test_content(object_name, method):
        test_content = TestMaker.generate_param_def(method['parameters'])

        function_call = TestMaker.generate_test_function_call(object_name, method)
        if method['returns'] == 'void':
            test_content += INTERSPACE + function_call + ';'
        else:
            if method['returns_pointer']:
                expected_value = 'nullptr'
            else:
                expected_value = method['returns'] + '{}'
            test_content += INTERSPACE + 'EXPECT_EQ({}, {});'.format(function_call, expected_value)

        return test_content

    @staticmethod
    def generate_test_function_call(object_name, method):
        return '{}.{}({})'.format(object_name, method['name'],
                                  TestMaker.generate_param_to_function(method['parameters']))

    @staticmethod
    def generate_param_def(parameters):
        param_str = ''
        for p in parameters:
            param_str += INTERSPACE + '{} {}{{}};\n'.format(p['raw_type'], p['name'])
        return param_str

    @staticmethod
    def generate_param_to_function(parameters):
        param_str = ''
        for p in parameters:
            if p['pointer']:
                param_str += '&'
            param_str += '{}, '.format(p['name'])
        return param_str[:-2]

    @staticmethod
    def test_class_name(class_name):
        return 'Test' + class_name[0].upper() + class_name[1:]

    @staticmethod
    def test_class_object(class_name):
        return 'm_' + class_name[0].lower() + class_name[1:]

    @staticmethod
    def save_file(file_path, cpp_str):
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with open(file_path, "w") as file:
            file.write(cpp_str)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='StubMaker - App will generate unit tests for every public function from C++ header file.')
    parser.add_argument('file_or_dir', help='C++ header file or directory with headers.')
    parser.add_argument('-o', dest='output_path', required=False, help='C++ implementation file path to save')

    args = parser.parse_args()
    test_generator = TestMaker(args)
