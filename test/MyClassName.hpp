#include <string>

class MyClassName
{
public:
    MyClassName() {};
    MyClassName(int a);
    ~MyClassName();

    void implementedFunction(const std::string& str, int p1, float p2) {}
    void functionVoid(const std::string& str, int p1, float p2);
    void functionCharPtrArg(char* str, int p1, float p2);
    void functionConstCharPtrArg(const char* str, int p1, float p2);
    int functionInt(const std::string& str, int p1, float p2);
    std::string functionStr(const std::string& str, int p1, float p2);
    int& functionIntRef(const std::string& str, int& p1, float* p2);
    int* functionIntPointer(const std::string& str, int& p1, float* p2);

protected:
    void implementedFunction2(const std::string& str, int p1, float p2) {}
    void functionVoid2(const std::string& str, int p1, float p2);
    int functionInt2(const std::string& str, int p1, float p2);
    int& functionIntRef2(const std::string& str, int& p1, float* p2);
    int* functionIntPointer2(const std::string& str, int& p1, float* p2);

private:
    void implementedFunction3(const std::string& str, int p1, float p2) {}
    void functionVoid3(const std::string& str, int p1, float p2);
    int functionInt3(const std::string& str, int p1, float p2);
    int& functionIntRef3(const std::string& str, int& p1, float* p2);
    int* functionIntPointer3(const std::string& str, int& p1, float* p2);
};
