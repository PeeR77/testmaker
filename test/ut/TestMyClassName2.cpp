#include <gtest/gtest.h>
#include "MyClassName2.hpp"


class TestMyClassName2 : public testing::Test
{
protected:
    MyClassName2 m_myClassName2;
};

TEST_F(TestMyClassName2, testImplementedFunction)
{
    std::string str{};
    int p1{};
    float p2{};
    m_myClassName2.implementedFunction(str, p1, p2);
}

TEST_F(TestMyClassName2, testFunctionVoid)
{
    std::string str{};
    int p1{};
    float p2{};
    m_myClassName2.functionVoid(str, p1, p2);
}

TEST_F(TestMyClassName2, testFunctionCharPtrArg)
{
    char str{};
    int p1{};
    float p2{};
    m_myClassName2.functionCharPtrArg(&str, p1, p2);
}

TEST_F(TestMyClassName2, testFunctionConstCharPtrArg)
{
    char str{};
    int p1{};
    float p2{};
    m_myClassName2.functionConstCharPtrArg(&str, p1, p2);
}

TEST_F(TestMyClassName2, testFunctionInt)
{
    std::string str{};
    int p1{};
    float p2{};
    EXPECT_EQ(m_myClassName2.functionInt(str, p1, p2), int{});
}

TEST_F(TestMyClassName2, testFunctionStr)
{
    std::string str{};
    int p1{};
    float p2{};
    EXPECT_EQ(m_myClassName2.functionStr(str, p1, p2), std::string{});
}

TEST_F(TestMyClassName2, testFunctionIntRef)
{
    std::string str{};
    int p1{};
    float p2{};
    EXPECT_EQ(m_myClassName2.functionIntRef(str, p1, &p2), int{});
}

TEST_F(TestMyClassName2, testFunctionIntPointer)
{
    std::string str{};
    int p1{};
    float p2{};
    EXPECT_EQ(m_myClassName2.functionIntPointer(str, p1, &p2), nullptr);
}

