#include <gtest/gtest.h>
#include "MyClassName.hpp"


class TestMyClassName : public testing::Test
{
protected:
    MyClassName m_myClassName;
};

TEST_F(TestMyClassName, testImplementedFunction)
{
    std::string str{};
    int p1{};
    float p2{};
    m_myClassName.implementedFunction(str, p1, p2);
}

TEST_F(TestMyClassName, testFunctionVoid)
{
    std::string str{};
    int p1{};
    float p2{};
    m_myClassName.functionVoid(str, p1, p2);
}

TEST_F(TestMyClassName, testFunctionCharPtrArg)
{
    char str{};
    int p1{};
    float p2{};
    m_myClassName.functionCharPtrArg(&str, p1, p2);
}

TEST_F(TestMyClassName, testFunctionConstCharPtrArg)
{
    char str{};
    int p1{};
    float p2{};
    m_myClassName.functionConstCharPtrArg(&str, p1, p2);
}

TEST_F(TestMyClassName, testFunctionInt)
{
    std::string str{};
    int p1{};
    float p2{};
    EXPECT_EQ(m_myClassName.functionInt(str, p1, p2), int{});
}

TEST_F(TestMyClassName, testFunctionStr)
{
    std::string str{};
    int p1{};
    float p2{};
    EXPECT_EQ(m_myClassName.functionStr(str, p1, p2), std::string{});
}

TEST_F(TestMyClassName, testFunctionIntRef)
{
    std::string str{};
    int p1{};
    float p2{};
    EXPECT_EQ(m_myClassName.functionIntRef(str, p1, &p2), int{});
}

TEST_F(TestMyClassName, testFunctionIntPointer)
{
    std::string str{};
    int p1{};
    float p2{};
    EXPECT_EQ(m_myClassName.functionIntPointer(str, p1, &p2), nullptr);
}

