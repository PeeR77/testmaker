#include "MyClassName2.hpp"




MyClassName2::MyClassName2(int /*a*/)
{
}

MyClassName2::~MyClassName2()
{
}

void MyClassName2::functionVoid(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
}

void MyClassName2::functionCharPtrArg(char * /*str*/, int /*p1*/, float /*p2*/)
{
}

void MyClassName2::functionConstCharPtrArg(const char * /*str*/, int /*p1*/, float /*p2*/)
{
}

int MyClassName2::functionInt(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
    int dummy{};
    return dummy;
}

std::string MyClassName2::functionStr(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
    std::string dummy{};
    return dummy;
}

int & MyClassName2::functionIntRef(const std::string & /*str*/, int & /*p1*/, float * /*p2*/)
{
    static int dummy{};
    return dummy;
}

int * MyClassName2::functionIntPointer(const std::string & /*str*/, int & /*p1*/, float * /*p2*/)
{
    return nullptr;
}

