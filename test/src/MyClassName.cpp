#include "MyClassName.hpp"




MyClassName::MyClassName(int /*a*/)
{
}

MyClassName::~MyClassName()
{
}

void MyClassName::functionVoid(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
}

void MyClassName::functionCharPtrArg(char * /*str*/, int /*p1*/, float /*p2*/)
{
}

void MyClassName::functionConstCharPtrArg(const char * /*str*/, int /*p1*/, float /*p2*/)
{
}

int MyClassName::functionInt(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
    int dummy{};
    return dummy;
}

std::string MyClassName::functionStr(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
    std::string dummy{};
    return dummy;
}

int & MyClassName::functionIntRef(const std::string & /*str*/, int & /*p1*/, float * /*p2*/)
{
    static int dummy{};
    return dummy;
}

int * MyClassName::functionIntPointer(const std::string & /*str*/, int & /*p1*/, float * /*p2*/)
{
    return nullptr;
}

